package com.aganglada.weather;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;


public class HandleXML 
{
   private String search, time, tempC, winK, precipMM, humidity, visibility, cloudcover, tempMax, tempMin, weatherDesc;
   private String urlString = null;
   private boolean objIsEmpty;
   private XmlPullParserFactory xmlFactoryObject;
   public volatile boolean parsingComplete = true;
   
   public HandleXML(String url)
   {
      this.urlString = url;
   }
   
   public String getSearch()
   {
      return search;
   }
   public String getTime()
   {
      return time;
   }
   
   public String getTempC()
   {
      return tempC;
   }
   
   public String getWinK()
   {
      return winK;
   }
   
   public String getprecipMM()
   {
	   return precipMM;
   }
   
   public String getHumudity()
   {
	   return humidity;
   }
   
   public String getVisibility()
   {
	   return visibility;
   }
   
   public String getCloudCover()
   {
	   return cloudcover;
   }
   
   public String getTempMax()
   {
	   return tempMax;
   }
   
   public String getTempMin()
   {
	   return tempMin;
   }
   
   public String getWeatherDesc()
   {
	   return weatherDesc;
   }
   
   public boolean objIsEmpty()
   {
	   return objIsEmpty;
   }
   
   public int getImage(String desc)
   {
	   int img = 0;
	   
	   switch (desc) 
	   {
			case "Sunny":
				img = R.drawable.sunny;
				break;
			case "Partly Cloudy":
				img = R.drawable.partly_cloudy;
				break;
			default:
				break;
	   }
	   
	   return img;
   }

   public void parseXMLAndStoreIt(XmlPullParser myParser) 
   {
      int event;
      String text = null;
      
      try 
      {
         event = myParser.getEventType();
         while (event != XmlPullParser.END_DOCUMENT) 
         {
            String name = myParser.getName();
            switch (event)
            {
               case XmlPullParser.START_TAG:
            	   break;
               case XmlPullParser.TEXT:
	               text = myParser.getText();
	               break;

               case XmlPullParser.END_TAG:
                  if (name.equals("query"))
                  {
                     search = text;
                  }
                  else if (name.equals("observation_time")) 
                  { 	
                     time = text;
                  }
                  else if (name.equals("temp_C"))
                  {
                     tempC = text;
                  }
                  else if (name.equals("windspeedKmph"))
                  {
                     winK = text;
                  }
                  else if (name.equals("precipMM"))
                  {
                	  precipMM = text;
                  }
                  else if (name.equals("humidity"))
                  {
                	  humidity = text;
                  }
                  else if (name.equals("visibility"))
                  {
                	  visibility = text;
                  }
                  else if (name.equals("tempMaxC"))
                  {
                	  tempMax = text;
                  }
                  else if (name.equals("tempMinC"))
                  {
                	  tempMin = text;
                  }
                  else if (name.equals("weatherDesc"))
                  {
                	  weatherDesc = text;
                  }
                  else
                  {
                  }
                  break;
                  }		 
                  event = myParser.next(); 

              }
              parsingComplete = false;
              
              if (search != "")
              {
             	 objIsEmpty = true;
              }
      } 
      catch (Exception e) 
      {
         e.printStackTrace();
      }

   }
   public void fetchXML()
   {
      Thread thread = new Thread(new Runnable()
      {
         @Override
         public void run() 
         {
            try 
            {
	           URL url = new URL(urlString);
	           HttpURLConnection conn = (HttpURLConnection) 
	           url.openConnection();
               conn.setReadTimeout(10000 /* milliseconds */);
               conn.setConnectTimeout(15000 /* milliseconds */);
               conn.setRequestMethod("GET");
               conn.setDoInput(true);
               conn.connect();
               InputStream stream = conn.getInputStream();

	           xmlFactoryObject = XmlPullParserFactory.newInstance();
	           XmlPullParser myparser = xmlFactoryObject.newPullParser();
	
	           myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES
	           , false);
	            myparser.setInput(stream, null);
	            parseXMLAndStoreIt(myparser);
	            stream.close();
            } 
            catch (Exception e) 
            {
               e.printStackTrace();
            }
        }
    });

    thread.start(); 
    
   }
}
