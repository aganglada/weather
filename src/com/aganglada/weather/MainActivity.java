package com.aganglada.weather;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends Activity 
{

	private final static String API_KEY = "d576c4827c3d5bf1697bd02718a4304e3609647d";
	private final static String FORMAT = "XML";
	private final static String BASE_URL = "http://api.worldweatheronline.com/free/v1/weather.ashx";
	public final static String EXTRA_MESSAGE = "com.aganglada.weather.MESSAGE";
	private LinearLayout result;
	private EditText search;
	private TextView city, time, tempC, maxTemp, minTemp, winK, precipMM, humidity, visibility, cloudcover, desc;
	private ImageView img;
	private HandleXML obj; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		result 		= (LinearLayout)findViewById(R.id.result);
		search 		= (EditText)findViewById(R.id.search);
		city 		= (TextView)findViewById(R.id.result_city);
		time 		= (TextView)findViewById(R.id.result_time);
		tempC 		= (TextView)findViewById(R.id.result_temp_c);
		winK 		= (TextView)findViewById(R.id.result_win_k);
		precipMM	= (TextView)findViewById(R.id.result_precipMM);
		humidity	= (TextView)findViewById(R.id.result_humidity);
		visibility	= (TextView)findViewById(R.id.result_visibility);
		cloudcover	= (TextView)findViewById(R.id.result_cloudcover);
		minTemp		= (TextView)findViewById(R.id.result_min_temp);
		maxTemp		= (TextView)findViewById(R.id.result_max_temp);
		desc		= (TextView)findViewById(R.id.result_desc);
		img			= (ImageView)findViewById(R.id.result_image);
		
		// Focus to Search
		search.requestFocus();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.result, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		if (id == R.id.action_settings) 
		{
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	public void open(View view) 
	{
		  String location = search.getText().toString();
	      String finalUrl = BASE_URL + "?q=" + location + "&format=" + FORMAT + "&key=" + API_KEY;
	      
	      obj = new HandleXML(finalUrl);
	      obj.fetchXML();
	      while(obj.parsingComplete);
	      
	      // Overview
	      city.setText(obj.getSearch());
	      time.setText(obj.getTime());
	      tempC.setText(obj.getTempC() + "ºC");
	      img.setImageResource(obj.getImage(obj.getWeatherDesc()));
	      
	      // Max/Min temp
	      desc.setText(obj.getWeatherDesc());
	      maxTemp.setText(obj.getTempMax() + "ºC max");
	      minTemp.setText(obj.getTempMin() + "ºC min");
	      
	      // Statistics
	      winK.setText("Win at " + obj.getWinK() + "km/h");
	      precipMM.setText(obj.getprecipMM() + "% of average rainfall");
	      humidity.setText(obj.getHumudity() + "% of moisture");
	      visibility.setText(obj.getVisibility() + "% of visibility");
	      cloudcover.setText(obj.getCloudCover() + "% of cloud cover");
	      
	      
	      result.setVisibility(LinearLayout.VISIBLE);
	      InputMethodManager imm = (InputMethodManager)getSystemService(
	    	      Context.INPUT_METHOD_SERVICE);
	    	imm.hideSoftInputFromWindow(search.getWindowToken(), 0);

	 }

}
